#fastapi endpoints
from fastapi import APIRouter, Depends #Depends allows FastAPI to create an instance
from queries.vacations import VacationIn, VacationRepository

router = APIRouter()        #hook up router to main app

@router.post("/vacations")
def create_vacation(
    vacation: VacationIn,
    repo: VacationRepository = Depends()): #makes the request body required

    repo.create(vacation)
    return vacation

# @router.get("/vacations")
# def get_vacation(vacation: VacationIn):
#     pass
