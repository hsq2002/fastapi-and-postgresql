# data for our endpoints, NOTHING TO DO WITH DATABASE
from pydantic import BaseModel
from typing import Optional #makes a data type optional
from datetime import date
from queries.pool import pool

class Thought(BaseModel):
    private_thoughts: str
    public_thoughts: str

class VacationIn(BaseModel):
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]

class VacationOut(BaseModel):
    id: int
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]

class VacationRepository:
    def create(self, vacation: VacationIn) -> VacationOut:
        with pool.connection() as conn:
        #connect tot he database
            #get a cursor (something to run SQL with)
            with conn.cursor() as db:
                #Run our INSERT statement
                result = db.execute(
                    """
                    INSERT INTO vacations
                        (name, from_date, to_date, thoughts)
                    VALUES
                        (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        vacation.name,
                        vacation.from_date,
                        vacation.to_date,
                        vacation.thoughts
                    ]
                )
                id = result.fetchone()[0]
                old_data = vacation.dict()
                return VacationOut(id=id, **old_data)
                #Return new data
